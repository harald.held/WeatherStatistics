#include "DbHandler.h"
#include "WeatherData.h"

#include <regex>

namespace
{
const std::string connectionTemplate = "postgresql://$url/postgres?user=postgres&password=postgres&sslmode=prefer";
}

DbHandler::DbHandler(const std::string &dbServerUrl)
    : serverUrl_(dbServerUrl)
    , connection_(
          std::make_unique<pqxx::connection>(std::regex_replace(connectionTemplate, std::regex("\\$url"), dbServerUrl)))
{
    createDatabase();
    initializeTableStructure();
}

void DbHandler::storeWeatherData(const WeatherData &data) const
{
    pqxx::work transaction(*connection_);

    transaction.exec0("INSERT INTO weatherdata.data VALUES " + data.toSqlValues());

    transaction.commit();
}

std::vector<WeatherData> DbHandler::dataOfLastNMinutes(int n) const
{
    std::vector<WeatherData> wds;

    pqxx::nontransaction query(*connection_);

    const std::string sql{ "SELECT * FROM weatherdata.data WHERE time > NOW() - interval '" + std::to_string(n) +
                           " minutes'" };

    auto result(query.exec(sql));

    for (const auto &r : result)
    {
        if (r[1].is_null() || r[2].is_null() || r[3].is_null() || r[4].is_null() || r[5].is_null() || r[6].is_null() ||
            r[7].is_null() || r[8].is_null() || r[9].is_null())
            continue;

        WeatherData wd;

        wd.insideTemperature       = r[1].as<double>();
        wd.outsideTemperature      = r[2].as<double>();
        wd.relativeHumidityInside  = r[3].as<int>();
        wd.relativeHumidityOutside = r[4].as<int>();
        wd.relativePressure        = r[5].as<double>();
        wd.windDirection           = r[6].as<double>();
        wd.windSpeed               = r[7].as<double>();
        wd.windGustSpeed           = r[8].as<double>();
        wd.rainFall                = r[9].as<double>();

        wds.push_back(std::move(wd));
    }

    return wds;
}

void DbHandler::createDatabase() const
{
    pqxx::work transaction(*connection_);

    transaction.exec0("CREATE SCHEMA IF NOT EXISTS weatherdata");
    transaction.exec0("CREATE EXTENSION IF NOT EXISTS timescaledb SCHEMA weatherdata CASCADE");

    transaction.commit();
}

void DbHandler::initializeTableStructure() const
{
    pqxx::work transaction(*connection_);

    transaction.exec0("CREATE TABLE IF NOT EXISTS weatherdata.data ("
                      "time TIMESTAMPTZ NOT NULL,"
                      "inside_temp DOUBLE PRECISION,"
                      "outside_temp DOUBLE PRECISION,"
                      "rel_hum_inside INT,"
                      "rel_hum_outside INT,"
                      "rel_pressure DOUBLE PRECISION,"
                      "wind_dir DOUBLE PRECISION,"
                      "wind_speed DOUBLE PRECISION,"
                      "wind_gust_speed DOUBLE PRECISION,"
                      "rain_fall DOUBLE PRECISION"
                      ")");

    transaction.exec1("SELECT weatherdata.create_hypertable('weatherdata.data', 'time', if_not_exists => TRUE)");

    transaction.commit();
}
