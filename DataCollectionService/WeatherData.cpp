#include "WeatherData.h"

#include <iomanip>
#include <ostream>
#include <sstream>

std::ostream &operator<<(std::ostream &out, const WeatherData &wd)
{
    out << "Weather data reading at " << std::put_time(std::localtime(&wd.timeOfReading), "%F %T") << "\n";
    out << "Value stored " << wd.lastValueStored << " minutes ago\n";
    out << "Inside temperature: " << wd.insideTemperature << "°C\n";
    out << "Outside temperature: " << wd.outsideTemperature << "°C\n";
    out << "Relative humidity inside: " << wd.relativeHumidityInside << "%\n";
    out << "Relative humidity outside: " << wd.relativeHumidityOutside << "%\n";
    out << "Relative pressure: " << wd.relativePressure << "hPa\n";
    out << "Wind direction in degress from north: " << wd.windDirection << "°\n";
    out << "Wind speed: " << wd.windSpeed << "km/h\n";
    out << "Wind gust speed: " << wd.windGustSpeed << "km/h\n";
    out << "Rain fall since last measurement: " << wd.rainFall << "mm\n";

    return out;
}

std::string WeatherData::toSqlValues() const
{
    std::ostringstream oss;

    oss << "('" << std::put_time(std::gmtime(&timeOfReading), "%F %T%z") << "',";

    if (insideTemperature >= -30 && insideTemperature <= 50)
    {
        oss << insideTemperature << ",";
    }
    else
    {
        oss << "NULL,";
    }

    if (outsideTemperature >= -30 && outsideTemperature <= 50)
    {
        oss << outsideTemperature << ",";
    }
    else
    {
        oss << "NULL,";
    }

    if (relativeHumidityInside >= 0 && relativeHumidityInside <= 100)
    {
        oss << relativeHumidityInside << ",";
    }
    else
    {
        oss << "NULL,";
    }

    if (relativeHumidityOutside >= 0 && relativeHumidityOutside <= 100)
    {
        oss << relativeHumidityOutside << ",";
    }
    else
    {
        oss << "NULL,";
    }

    if (relativePressure >= 900 && relativePressure <= 1200)
    {
        oss << relativePressure << ",";
    }
    else
    {
        oss << "NULL,";
    }

    if (windDirection >= 0 && windDirection <= 360)
    {
        oss << windDirection << ",";
    }
    else
    {
        oss << "NULL,";
    }

    if (windSpeed >= 0 && windSpeed <= 410)
    {
        oss << windSpeed << ",";
    }
    else
    {
        oss << "NULL,";
    }

    if (windGustSpeed >= 0 && windGustSpeed <= 410)
    {
        oss << windGustSpeed << ",";
    }
    else
    {
        oss << "NULL,";
    }

    if (rainFall >= 0 && rainFall <= 100)
    {
        oss << rainFall << ")";
    }
    else
    {
        oss << "NULL)";
    }

    return oss.str();
}
