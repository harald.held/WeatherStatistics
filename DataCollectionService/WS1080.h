#ifndef WS1080_H
#define WS1080_H

#include "WeatherData.h"

#include <optional>

#include <libusb.h>

struct libusb_device_handle;

class WS1080 final
{
public:
    WS1080();
    ~WS1080();

    bool isConnected() const;

    /**
     * @brief currentData returns the station's current data, if any
     *
     * If something goes wrong or the station's current address has not changed since the last reading, this returns
     * nothing.
     *
     * @return Current data reading or nothing
     */
    std::optional<WeatherData> currentData() const;

private:
    libusb_device_handle *usbDevice_ = nullptr;

    void openUsbDevice();
    void closeUsbDevice();
    void readData(uint16_t address, uint8_t *data, uint16_t size) const;
};

#endif // WS1080_H
