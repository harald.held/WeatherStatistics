#include "DbHandler.h"
#include "WS1080.h"

#include <chrono>
#include <cmath>
#include <iostream>
#include <thread>

bool isOutlier(const WeatherData &wd, const DbHandler &dbHandler)
{
    auto lastValues = dbHandler.dataOfLastNMinutes(60);

    // if there was at most one value within the last 60 minutes, always accept the new value
    if (lastValues.size() <= 1)
        return false;

    const double numSamples    = static_cast<double>(lastValues.size());
    const auto & previousValue = lastValues.back();

    // compute the average temperatures, humidities, pressures and their unbiased sample variances
    double avgInsideTemp      = 0.;
    double avgOutsideTemp     = 0.;
    double avgInsideHumidity  = 0.;
    double avgOutsideHumidity = 0.;
    double avgPressure        = 0.;

    for (const auto &lv : lastValues)
    {
        avgInsideTemp += lv.insideTemperature;
        avgOutsideTemp += lv.outsideTemperature;
        avgInsideHumidity += lv.relativeHumidityInside;
        avgOutsideHumidity += lv.relativeHumidityOutside;
        avgPressure += lv.relativePressure;
    }

    avgInsideTemp /= numSamples;
    avgOutsideTemp /= numSamples;
    avgInsideHumidity /= numSamples;
    avgOutsideHumidity /= numSamples;
    avgPressure /= numSamples;

    double varianceInsideTemp      = 0.;
    double varianceOutsideTemp     = 0.;
    double varianceInsideHumidity  = 0.;
    double varianceOutsideHumidity = 0.;
    double variancePressure        = 0.;

    const double lastInsideTemp      = previousValue.insideTemperature;
    const double lastOutsideTemp     = previousValue.outsideTemperature;
    const double lastInsideHumidity  = previousValue.relativeHumidityInside;
    const double lastOutsideHumidity = previousValue.relativeHumidityOutside;
    const double lastPressure        = previousValue.relativePressure;

    for (const auto &lv : lastValues)
    {
        varianceInsideTemp += std::pow(lv.insideTemperature - avgInsideTemp, 2.);
        varianceOutsideTemp += std::pow(lv.outsideTemperature - avgOutsideTemp, 2.);
        varianceInsideHumidity += std::pow(lv.relativeHumidityInside - avgInsideHumidity, 2.);
        varianceOutsideHumidity += std::pow(lv.relativeHumidityOutside - avgOutsideHumidity, 2.);
        variancePressure += std::pow(lv.relativePressure - avgPressure, 2.);
    }

    varianceInsideTemp /= numSamples - 1.;
    varianceOutsideTemp /= numSamples - 1.;
    varianceInsideHumidity /= numSamples - 1.;
    varianceOutsideHumidity /= numSamples - 1.;
    variancePressure /= numSamples - 1.;

    // accept if new value is within 1.5 * variation and close enough to avg
    if (std::pow(wd.insideTemperature - avgInsideTemp, 2.) > 1.5 * varianceInsideTemp &&
        std::fabs(wd.insideTemperature - lastInsideTemp) > 3.)
    {
        std::clog << "Classified as outlier because of inside temperature: " << wd.insideTemperature << ", "
                  << avgInsideTemp << ", " << 1.5 * varianceInsideTemp << std::endl;
        return true;
    }
    if (std::pow(wd.outsideTemperature - avgOutsideTemp, 2.) > 1.5 * varianceOutsideTemp &&
        std::fabs(wd.outsideTemperature - lastOutsideTemp) > 3.)
    {
        std::clog << "Classified as outlier because of outside temperature: " << wd.outsideTemperature << ", "
                  << avgOutsideTemp << ", " << 1.5 * varianceOutsideTemp << std::endl;
        return true;
    }
    if (std::pow(wd.relativeHumidityInside - avgInsideHumidity, 2.) > 1.5 * varianceInsideHumidity &&
        std::fabs(wd.relativeHumidityInside - lastInsideHumidity) > 5.)
    {
        std::clog << "Classified as outlier because of inside humidity: " << wd.relativeHumidityInside << ", "
                  << avgInsideHumidity << ", " << 1.5 * varianceInsideHumidity << std::endl;
        return true;
    }
    if (std::pow(wd.relativeHumidityOutside - avgOutsideHumidity, 2.) > 1.5 * varianceOutsideHumidity &&
        std::fabs(wd.relativeHumidityOutside - lastOutsideHumidity) > 5.)
    {
        std::clog << "Classified as outlier because of outside humidity: " << wd.relativeHumidityOutside << ", "
                  << avgOutsideHumidity << ", " << 1.5 * varianceOutsideHumidity << std::endl;
        return true;
    }
    if (std::pow(wd.relativePressure - avgPressure, 2.) > 1.5 * variancePressure &&
        std::fabs(wd.relativePressure - lastPressure) > 3.)
    {
        std::clog << "Classified as outlier because of pressure: " << wd.relativePressure << ", " << avgPressure << ", "
                  << 1.5 * variancePressure << std::endl;
        return true;
    }

    return false;
}

int main()
{
    DbHandler dbHandler("localhost");
    WS1080    ws;

    while (true)
    {
        // 60s periodic reads reduce the chance of USB lock ups according to
        // https://github.com/weewx/weewx/wiki/FineOffset-USB-lockup
        // Also, initially, sleep for a minute to avoid weird spikes in data when rebooting the RaspberriPi
        std::this_thread::sleep_for(std::chrono::seconds(60));

        auto currentData = ws.currentData();

        if (currentData.has_value())
        {
            if (!isOutlier(*currentData, dbHandler))
            {
                std::clog << *currentData << std::endl << std::endl;
                dbHandler.storeWeatherData(*currentData);
            }
            else
            {
                std::clog << "####### Outlier detected #######" << std::endl;
                std::clog << *currentData << std::endl << std::endl;
            }
        }
        else
        {
            std::cerr << "No data read\n\n";
        }
    }
}
