#ifndef WEATHERDATA_H
#define WEATHERDATA_H

#include <ctime>
#include <iosfwd>
#include <string>

/// Represents weather data obtained from one reading call to the weather station.
struct WeatherData
{
    std::time_t timeOfReading = std::time(nullptr);

    /// Time in minutes passed since the last set of data was stored.
    int lastValueStored = 0;

    /// Inside temperature in °C.
    double insideTemperature = 0.;

    /// Outside temperature in °C.
    double outsideTemperature = 0.;

    /// Relative insisde humidity in %.
    int relativeHumidityInside = 0;

    /// Relative outside humidity in %.
    int relativeHumidityOutside = 0;

    /// Relative pressure at the station's location in hPa.
    double relativePressure = 0;

    /// Wind direction angle in degress from north (22.5° increments).
    double windDirection = 0.;

    /// Wind speed in km/h.
    double windSpeed = 0.;

    /// Wind gust speed in km/h.
    double windGustSpeed = 0.;

    /// Rain fall since last data storing in mm.
    double rainFall = 0.;

    std::string toSqlValues() const;
};

std::ostream &operator<<(std::ostream &out, const WeatherData &wd);

#endif // WEATHERDATA_H
