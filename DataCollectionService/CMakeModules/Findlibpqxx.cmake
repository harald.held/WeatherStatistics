find_path(LIBPQXX_INCLUDE_DIR pqxx/pqxx
    HINTS /usr/include
)

find_library(LIBPQXX_LIBRARY pqxx
    HINTS /usr/lib
)

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(libpqxx DEFAULT_MSG
    LIBPQXX_LIBRARY LIBPQXX_INCLUDE_DIR
)

set(LIBPQXX_LIBRARIES ${LIBPQXX_LIBRARY})
set(LIBPQXX_INCLUDE_DIRS ${LIBPQXX_INCLUDE_DIR})
