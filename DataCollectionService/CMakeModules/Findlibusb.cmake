find_path(LIBUSB_INCLUDE_DIR libusb.h
    HINTS /usr/include/libusb-1.0
)

find_library(LIBUSB_LIBRARY usb-1.0
    HINTS /usr/lib
)

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(libusb DEFAULT_MSG
    LIBUSB_LIBRARY LIBUSB_INCLUDE_DIR
)

set(LIBUSB_LIBRARIES ${LIBUSB_LIBRARY})
set(LIBUSB_INCLUDE_DIRS ${LIBUSB_INCLUDE_DIR})
