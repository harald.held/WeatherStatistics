#ifndef DBHANDLER_H
#define DBHANDLER_H

#include <memory>
#include <string>
#include <vector>

#include <pqxx/pqxx>

class WeatherData;

class DbHandler final
{
public:
    DbHandler(const std::string &dbServerUrl);

    void                     storeWeatherData(const WeatherData &data) const;
    std::vector<WeatherData> dataOfLastNMinutes(int n) const;

private:
    const std::string                 serverUrl_;
    std::unique_ptr<pqxx::connection> connection_ = nullptr;

    void createDatabase() const;
    void initializeTableStructure() const;
};

#endif // DBHANDLER_H
