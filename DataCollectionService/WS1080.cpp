#include "WS1080.h"

#include <cstdio>
#include <cstring>
#include <iostream>

namespace
{
uint16_t lastCurrentAddress        = 0xffff;
int      sameCurrentAddressCounter = 0;

auto interpretTemperature = [](uint8_t highByte, uint8_t lowByte) {
    int mergedByte = (highByte << 8) | (lowByte & 0xff);

    if (mergedByte & 0x8000)
    {
        mergedByte = mergedByte & 0x7FFF;
        mergedByte = 0 - mergedByte;
    }
    else
    {
        mergedByte = mergedByte ^ 0x0000;
    }

    return 0.1 * mergedByte;
};

auto interpretPressure      = [](uint8_t highByte, uint8_t lowByte) { return (lowByte + (highByte << 8)) * 0.1; };
auto interpretWindDirection = [](uint8_t windDir) { return 22.5 * windDir; };
auto interpretWindSpeed     = [](uint8_t windSpeed) { return windSpeed * 0.1 * 3600. / 1000.; };
auto interpretRainFall      = [](uint8_t highByte, uint8_t lowByte) { return (lowByte + (highByte << 8)) * 0.3; };
}

WS1080::WS1080()
{
    libusb_init(nullptr);

    openUsbDevice();
}

WS1080::~WS1080()
{
    closeUsbDevice();
    libusb_exit(nullptr);
}

bool WS1080::isConnected() const
{
    return usbDevice_ != nullptr;
}

std::optional<WeatherData> WS1080::currentData() const
{
    if (!isConnected())
    {
        std::cerr << "No USB connection to station\n";
        const_cast<WS1080 *>(this)->openUsbDevice();
        return {};
    }

    WeatherData cd;

    uint8_t currentData[16];
    uint8_t previousData[16];

    uint16_t addressCurrent;
    readData(30, reinterpret_cast<unsigned char *>(&addressCurrent), sizeof(addressCurrent));

    if (addressCurrent == lastCurrentAddress)
    {
        ++sameCurrentAddressCounter;

        if (sameCurrentAddressCounter > 60)
        {
            // this can only mean we are stuck again due to this USB lock-up issue with this particular station
            std::cerr << "It seems that the station is no longer logging data ... reset it by completely removing any "
                         "power supply and connect it again!\n";
            return {};
        }

//        if (addressCurrent == lastCurrentAddress)
//            return {};
    }
    else
    {
        sameCurrentAddressCounter = 0;
        lastCurrentAddress        = addressCurrent;
    }

    // need this for rain computation
    uint16_t addressPrevious;
    addressPrevious = addressCurrent - 0x10;

    if (addressPrevious < 0x100)
    {
        addressPrevious = 0x100 + (addressPrevious & 0xffff);
    }

    readData(addressCurrent, currentData, 16);
    readData(addressPrevious, previousData, 16);

    cd.lastValueStored         = static_cast<int>(currentData[0]);
    cd.insideTemperature       = interpretTemperature(currentData[0x3], currentData[0x2]);
    cd.outsideTemperature      = interpretTemperature(currentData[0x6], currentData[0x5]);
    cd.relativeHumidityInside  = static_cast<int>(currentData[1]);
    cd.relativeHumidityOutside = static_cast<int>(currentData[4]);
    cd.relativePressure        = interpretPressure(currentData[0x8], currentData[0x7]);
    cd.windDirection           = interpretWindDirection(currentData[0xc]);
    cd.windSpeed               = interpretWindSpeed(currentData[0x9]);
    cd.windGustSpeed           = interpretWindSpeed(currentData[0xa]);
    cd.rainFall =
        interpretRainFall(currentData[0xe], currentData[0xd]) - interpretRainFall(previousData[0xe], previousData[0xd]);

    return cd;
}

void WS1080::readData(uint16_t address, uint8_t *data, uint16_t size) const
{
    if (!usbDevice_)
        return;

    char     cmd[9];
    uint8_t  buf[0x20];
    uint16_t c = sizeof(buf);

    std::sprintf(cmd, "\xA1%c%c%c\xA1%c%c%c", address / 256, address % 256, c, address / 256, address % 256, c);

    int r = libusb_control_transfer(usbDevice_,
                                    LIBUSB_ENDPOINT_OUT | LIBUSB_REQUEST_TYPE_CLASS | LIBUSB_RECIPIENT_INTERFACE, 0x9,
                                    0x200, 0, reinterpret_cast<unsigned char *>(cmd), sizeof(cmd) - 1, 1000);

    if (r > 0)
    {
        int actualLen = 0;
        int state     = libusb_interrupt_transfer(usbDevice_, 0x81, buf, c, &actualLen, 1000);

        if (state < 0)
        {
            std::cerr << "Error reading from device (" << state << "): " << libusb_error_name(state) << "\n";
        }
        else
        {
            memcpy(data, buf, size);
        }
    }
    else
    {
        std::cerr << "libusb_control_transfer error (" << r << "): " << libusb_error_name(r) << "\n";
    }
}

void WS1080::openUsbDevice()
{
    usbDevice_ = libusb_open_device_with_vid_pid(nullptr, 0x1941, 0x8021);

    if (!usbDevice_)
        return;

    int kernelActive = libusb_kernel_driver_active(usbDevice_, 0);

    if (kernelActive == 1)
    {
        libusb_detach_kernel_driver(usbDevice_, 0);
    }

    libusb_claim_interface(usbDevice_, 0);
}

void WS1080::closeUsbDevice()
{
    libusb_close(usbDevice_);
    usbDevice_ = nullptr;
}
